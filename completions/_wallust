#compdef wallust

autoload -U is-at-least

_wallust() {
    typeset -A opt_args
    typeset -a _arguments_options
    local ret=1

    if is-at-least 5.2; then
        _arguments_options=(-s -S -C)
    else
        _arguments_options=(-s -C)
    fi

    local context curcontext="$curcontext" state line
    _arguments "${_arguments_options[@]}" \
'(-s --skip-sequences)*-i+[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-s --skip-sequences)*--ignore-sequence=[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-d --config-dir)-C+[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-d --config-dir)--config-file=[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-C --config-file)-d+[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'(-C --config-file)--config-dir=[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'-q[Don'\''t print anything]' \
'--quiet[Don'\''t print anything]' \
'(-u --update-current -i --ignore-sequence)-s[Skip setting terminal sequences]' \
'(-u --update-current -i --ignore-sequence)--skip-sequences[Skip setting terminal sequences]' \
'-T[Skip templating process]' \
'--skip-templates[Skip templating process]' \
'(-s --skip-sequences)-u[Only update the current terminal]' \
'(-s --skip-sequences)--update-current[Only update the current terminal]' \
'(-C --config-file -d --config-dir)-N[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'(-C --config-file -d --config-dir)--no-config[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'-h[Print help (see more with '\''--help'\'')]' \
'--help[Print help (see more with '\''--help'\'')]' \
'-V[Print version]' \
'--version[Print version]' \
":: :_wallust_commands" \
"*::: :->wallust" \
&& ret=0
    case $state in
    (wallust)
        words=($line[1] "${words[@]}")
        (( CURRENT += 1 ))
        curcontext="${curcontext%:*:*}:wallust-command-$line[1]:"
        case $line[1] in
            (run)
_arguments "${_arguments_options[@]}" \
'-a+[Alpha *template variable* value, used only for templating (default is 100)]:ALPHA: ' \
'--alpha=[Alpha *template variable* value, used only for templating (default is 100)]:ALPHA: ' \
'-b+[Choose which backend to use (overwrites config)]:BACKEND:((full\:"Read and return the whole image pixels (more precision, slower)"
resized\:"Resizes the image before parsing, mantaining it'\''s aspect ratio"
wal\:"Uses image magick \`convert\` to generate the colors, like pywal"
thumb\:"Faster algo hardcoded to 512x512 (no ratio respected)"
fastresize\:"A much faster resize algo that uses SIMD. For some reason it fails on some images where \`resized\` doesn'\''t, for this reason it doesn'\''t *replace* but rather it'\''s a new option"
kmeans\:"Kmeans is an algo that divides and picks pixels all around the image, Requires more tweaking and more in depth testing but, for the most part, "it just werks""))' \
'--backend=[Choose which backend to use (overwrites config)]:BACKEND:((full\:"Read and return the whole image pixels (more precision, slower)"
resized\:"Resizes the image before parsing, mantaining it'\''s aspect ratio"
wal\:"Uses image magick \`convert\` to generate the colors, like pywal"
thumb\:"Faster algo hardcoded to 512x512 (no ratio respected)"
fastresize\:"A much faster resize algo that uses SIMD. For some reason it fails on some images where \`resized\` doesn'\''t, for this reason it doesn'\''t *replace* but rather it'\''s a new option"
kmeans\:"Kmeans is an algo that divides and picks pixels all around the image, Requires more tweaking and more in depth testing but, for the most part, "it just werks""))' \
'-c+[Choose which colorspace to use (overwrites config)]:COLORSPACE:((lab\:"Uses Cie L*a*b color space"
labmixed\:"Variant of \`lab\` that mixes the colors gathered, if not enough colors it fallbacks to usual lab (not recommended in small images)"
lch\:"CIE Lch, you can understand this color space like LAB but with chrome and hue added. Could help when sorting"
lchmixed\:"CIE Lch, you can understand this color space like LAB but with chrome and hue added. Could help when sorting"))' \
'--colorspace=[Choose which colorspace to use (overwrites config)]:COLORSPACE:((lab\:"Uses Cie L*a*b color space"
labmixed\:"Variant of \`lab\` that mixes the colors gathered, if not enough colors it fallbacks to usual lab (not recommended in small images)"
lch\:"CIE Lch, you can understand this color space like LAB but with chrome and hue added. Could help when sorting"
lchmixed\:"CIE Lch, you can understand this color space like LAB but with chrome and hue added. Could help when sorting"))' \
'-f+[Choose which fallback generation method to use (overwrites config)]:FALLBACK_GENERATOR:((interpolate\:"uses \[\`interpolate\`\]"
complementary\:"uses \[\`complementary\`\]"))' \
'--fallback-generator=[Choose which fallback generation method to use (overwrites config)]:FALLBACK_GENERATOR:((interpolate\:"uses \[\`interpolate\`\]"
complementary\:"uses \[\`complementary\`\]"))' \
'-p+[Choose which palette to use (overwrites config)]:PALETTE:((dark\:"8 dark colors, dark background and light contrast"
dark16\:"Same as \`dark\` but uses the 16 colors trick"
darkcomp\:"This is a \`dark\` variant that changes all colors to it'\''s complementary counterpart, giving the feeling of a '\''new palette'\'' but that still makes sense with the image provided"
darkcomp16\:"16 variation of the dark complementary variant"
harddark\:"Same as \`dark\` with hard hue colors"
harddark16\:"Harddark with 16 color variation"
harddarkcomp\:"complementary colors variation of harddark scheme"
harddarkcomp16\:"complementary colors variation of harddark scheme"
light\:"Light bg, dark fg"
light16\:"Same as \`light\` but uses the 16 color trick"
lightcomp\:"complementary colors variation of light"
lightcomp16\:"complementary colors variation of light with the 16 color variation"
softdark\:"Variant of softlight, uses the lightest colors and a dark background (could be interpreted as \`dark\` inversed)"
softdark16\:"softdark with 16 color variation"
softdarkcomp\:"complementary variation for softdark"
softdarkcomp16\:"complementary variation for softdark with the 16 color variation"
softlight\:"Light with soft pastel colors, counterpart of \`harddark\`"
softlight16\:"softlight with 16 color variation"
softlightcomp\:"softlight with complementary colors"
softlightcomp16\:"softlight with complementary colors with 16 colors"))' \
'--palette=[Choose which palette to use (overwrites config)]:PALETTE:((dark\:"8 dark colors, dark background and light contrast"
dark16\:"Same as \`dark\` but uses the 16 colors trick"
darkcomp\:"This is a \`dark\` variant that changes all colors to it'\''s complementary counterpart, giving the feeling of a '\''new palette'\'' but that still makes sense with the image provided"
darkcomp16\:"16 variation of the dark complementary variant"
harddark\:"Same as \`dark\` with hard hue colors"
harddark16\:"Harddark with 16 color variation"
harddarkcomp\:"complementary colors variation of harddark scheme"
harddarkcomp16\:"complementary colors variation of harddark scheme"
light\:"Light bg, dark fg"
light16\:"Same as \`light\` but uses the 16 color trick"
lightcomp\:"complementary colors variation of light"
lightcomp16\:"complementary colors variation of light with the 16 color variation"
softdark\:"Variant of softlight, uses the lightest colors and a dark background (could be interpreted as \`dark\` inversed)"
softdark16\:"softdark with 16 color variation"
softdarkcomp\:"complementary variation for softdark"
softdarkcomp16\:"complementary variation for softdark with the 16 color variation"
softlight\:"Light with soft pastel colors, counterpart of \`harddark\`"
softlight16\:"softlight with 16 color variation"
softlightcomp\:"softlight with complementary colors"
softlightcomp16\:"softlight with complementary colors with 16 colors"))' \
'--saturation=[Add saturation from 1% to 100% (overwrites config)]:SATURATION: ' \
'-t+[Choose a custom threshold, between 1 and 100 (overwrites config)]:THRESHOLD: ' \
'--threshold=[Choose a custom threshold, between 1 and 100 (overwrites config)]:THRESHOLD: ' \
'(-s --skip-sequences)*-i+[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-s --skip-sequences)*--ignore-sequence=[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-d --config-dir)-C+[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-d --config-dir)--config-file=[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-C --config-file)-d+[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'(-C --config-file)--config-dir=[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'-k[Ensure a readable contrast by checking colors in reference to the background (overwrites config)]' \
'--check-contrast[Ensure a readable contrast by checking colors in reference to the background (overwrites config)]' \
'-n[Don'\''t cache the results]' \
'--no-cache[Don'\''t cache the results]' \
'-w[Generates colors even if there is a cache version of it]' \
'--overwrite-cache[Generates colors even if there is a cache version of it]' \
'-q[Don'\''t print anything]' \
'--quiet[Don'\''t print anything]' \
'(-u --update-current -i --ignore-sequence)-s[Skip setting terminal sequences]' \
'(-u --update-current -i --ignore-sequence)--skip-sequences[Skip setting terminal sequences]' \
'-T[Skip templating process]' \
'--skip-templates[Skip templating process]' \
'(-s --skip-sequences)-u[Only update the current terminal]' \
'(-s --skip-sequences)--update-current[Only update the current terminal]' \
'(-C --config-file -d --config-dir)-N[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'(-C --config-file -d --config-dir)--no-config[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'-h[Print help (see more with '\''--help'\'')]' \
'--help[Print help (see more with '\''--help'\'')]' \
':file -- Path to an image or json theme to use:_files' \
&& ret=0
;;
(cs)
_arguments "${_arguments_options[@]}" \
'-f+[Specify a custom format. Without this option, wallust will sequentially try to decode it by trying one by one]:FORMAT:((pywal\:"uses the wal colorscheme format, see <https\://github.com/dylanaraps/pywal/tree/master/pywal/colorschemes>"
terminal-sexy\:"uses <https\://terminal.sexy> JSON export"
wallust\:"cached wallust files"))' \
'--format=[Specify a custom format. Without this option, wallust will sequentially try to decode it by trying one by one]:FORMAT:((pywal\:"uses the wal colorscheme format, see <https\://github.com/dylanaraps/pywal/tree/master/pywal/colorschemes>"
terminal-sexy\:"uses <https\://terminal.sexy> JSON export"
wallust\:"cached wallust files"))' \
'(-s --skip-sequences)*-i+[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-s --skip-sequences)*--ignore-sequence=[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-d --config-dir)-C+[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-d --config-dir)--config-file=[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-C --config-file)-d+[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'(-C --config-file)--config-dir=[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'-q[Don'\''t print anything]' \
'--quiet[Don'\''t print anything]' \
'(-u --update-current -i --ignore-sequence)-s[Skip setting terminal sequences]' \
'(-u --update-current -i --ignore-sequence)--skip-sequences[Skip setting terminal sequences]' \
'-T[Skip templating process]' \
'--skip-templates[Skip templating process]' \
'(-s --skip-sequences)-u[Only update the current terminal]' \
'(-s --skip-sequences)--update-current[Only update the current terminal]' \
'(-C --config-file -d --config-dir)-N[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'(-C --config-file -d --config-dir)--no-config[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'-h[Print help (see more with '\''--help'\'')]' \
'--help[Print help (see more with '\''--help'\'')]' \
':file -- Path to the file that has a colorscheme:_files' \
&& ret=0
;;
(theme)
_arguments "${_arguments_options[@]}" \
'(-s --skip-sequences)*-i+[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-s --skip-sequences)*--ignore-sequence=[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-d --config-dir)-C+[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-d --config-dir)--config-file=[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-C --config-file)-d+[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'(-C --config-file)--config-dir=[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'-p[Only preview the selected theme]' \
'--preview[Only preview the selected theme]' \
'-q[Don'\''t print anything]' \
'--quiet[Don'\''t print anything]' \
'(-u --update-current -i --ignore-sequence)-s[Skip setting terminal sequences]' \
'(-u --update-current -i --ignore-sequence)--skip-sequences[Skip setting terminal sequences]' \
'-T[Skip templating process]' \
'--skip-templates[Skip templating process]' \
'(-s --skip-sequences)-u[Only update the current terminal]' \
'(-s --skip-sequences)--update-current[Only update the current terminal]' \
'(-C --config-file -d --config-dir)-N[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'(-C --config-file -d --config-dir)--no-config[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'-h[Print help]' \
'--help[Print help]' \
':theme -- A custom built in theme to choose from:(dkeg-petal base16-paraiso sexy-s3r0-modified sexy-rasi solarized-dark sexy-muse base16-rebecca base16-classic-dark dkeg-leaf dkeg-harbing base16-atelier-forest-light sexy-simple_rainbow base16-bright base16-embers base16-chalk dkeg-flapr sexy-tangoesque base16-twilight sexy-visibone base16-brewer sexy-x-dotshare sexy-astromouse dkeg-bulb base16-summerfruit-light dkeg-chaires base16-black-metal-funeral base16-black-metal-burzum base16-codeschool base16-cupcake tempus_autumn dkeg-link sexy-gslob-nature-suede sexy-hybrid base16-materialer-dark dkeg-view dkeg-blumune base16-zenburn base16-gruvbox-pale dkeg-sprout sexy-mikado dkeg-scag base16-macintosh base16-black-metal-khold base16-atelier-plateau-dark base16-unikitty-light base16-google-light base16-materialer-light base16-pop base16-flat base16tooth base16-black-metal-venom base16-atelier-sulphurpool-dark base16-atelier-estuary-light sexy-zenburn base16-eighties sexy-eqie6 base16-3024 sexy-dwmrob base16-black-metal-marduk dkeg-brownstone dkeg-escen sexy-user-77-mashup-colors base16-mocha base16-mexico tempus_dusk base16-grayscale-light dkeg-novmbr dkeg-urban tempus_rift sexy-thwump dkeg-transposet tempus_future dkeg-stv base16-railscasts sexy-mikazuki base16-tomorrow base16-unikitty-dark sexy-jasonwryan dkeg-tealights base16-solarflare dkeg-raild sexy-invisibone base16-material-palenight dkeg-scape base16-black-metal-gorgoroth base16-solarized-light base16-black-metal-bathory base16-outrun dkeg-bark dkeg-spire sexy-sexcolors base16-woodland dkeg-simplicity base16-monokai base16-mellow-purple base16-xcode-dusk base16-porple base16-isotope dkeg-fendr dkeg-sundr sexy-nancy base16-classic-light dkeg-5725 base16-atelier-lakeside-dark sexy-navy-and-ivory 3024-light monokai base16-oceanicnext sexy-euphrasia sexy-visibone-alt-2 dkeg-mattd base16-atelier-seaside-light base16-atelier-savanna-light base16-atelier-heath-light dkeg-vans dkeg-coco sexy-gjm sexy-kasugano base16-atelier-sulphurpool-light base16-nord base16-black-metal-nile base16-tomorrow-night sexy-material base16-cupertino sexy-tlh srcery base16-phd base16-github tempus_summer base16-gruvbox-soft-dark base16-bespin sexy-rydgel dkeg-forst dkeg-slate sexy-theme2 base16-marrakesh sexy-colorfulcolors sexy-neon dkeg-diner base16-apathy sexy-gnometerm sexy-parker_brothers sexy-mostly-bright sexy-doomicideocean base16-atelier-dune-light zenburn sexy-rezza dkeg-relax ashes-light dkeg-provrb dkeg-skigh base16-tube dkeg-amiox sexy-numixdarkest base16-atelier-forest-dark sexy-pretty-and-pastel sexy-splurge base16-irblack base16-materia base16-gruvbox-hard-light dkeg-owl base16-atelier-heath-dark sexy-insignificato gruvbox darktooth sexy-trim-yer-beard sexy-dotshare dkeg-designr dkeg-poly base16-brushtrees base16-dracula base16-atelier-lakeside-light tempus_totus base16-atelier-cave-dark sexy-hund tempus_fugit base16-black-metal-mayhem base16-default-light dkeg-shade base16-default-dark vscode base16-atelier-plateau-light base16-hopscotch base16-grayscale-dark base16-atelier-estuary-dark base16-icy 3024-dark sexy-cloud rose-pine sexy-phrak1 base16-snazzy rose-pine-moon sexy-deafened dkeg-fury dkeg-blok base16-summerfruit-dark base16-pico sexy-sweetlove sexy-belge dkeg-victory sexy-bitmute base16-spacemacs sexy-orangish tempus_dawn dkeg-blend dkeg-book github base16-circus base16-gruvbox-medium-dark dkeg-prevail dkeg-depth base16-black-metal-immortal dkeg-soundwave sexy-gotham base16-atelier-seaside-dark tempus_warp base16-onedark dkeg-pastely base16-harmonic-light sexy-vacuous2 tempus_winter base16-atelier-savanna-dark base16-ashes rose-pine-dawn sexy-swayr sexy-digerati base16-google-dark sexy-monokai dkeg-branch dkeg-squares sexy-tartan tempus_spring base16-one base16-black-metal tempus_past base16-gruvbox-medium-light dkeg-bluetype base16-atelier-cave-light dkeg-subtle solarized-light base16-gruvbox-hard-dark base16-atelier-dune-dark sexy-tango dkeg-wintry base16-greenscreen base16-harmonic-dark base16-ocean dkeg-parkd sexy-derp dkeg-paints ashes-dark dkeg-kit base16-solarized-dark base16-seti sexy-dawn base16-gruvbox-soft-light hybrid-material dkeg-corduroy dkeg-traffic base16-shapeshifter base16-material random)' \
&& ret=0
;;
(migrate)
_arguments "${_arguments_options[@]}" \
'(-s --skip-sequences)*-i+[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-s --skip-sequences)*--ignore-sequence=[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-d --config-dir)-C+[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-d --config-dir)--config-file=[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-C --config-file)-d+[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'(-C --config-file)--config-dir=[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'-q[Don'\''t print anything]' \
'--quiet[Don'\''t print anything]' \
'(-u --update-current -i --ignore-sequence)-s[Skip setting terminal sequences]' \
'(-u --update-current -i --ignore-sequence)--skip-sequences[Skip setting terminal sequences]' \
'-T[Skip templating process]' \
'--skip-templates[Skip templating process]' \
'(-s --skip-sequences)-u[Only update the current terminal]' \
'(-s --skip-sequences)--update-current[Only update the current terminal]' \
'(-C --config-file -d --config-dir)-N[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'(-C --config-file -d --config-dir)--no-config[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'-h[Print help]' \
'--help[Print help]' \
&& ret=0
;;
(debug)
_arguments "${_arguments_options[@]}" \
'(-s --skip-sequences)*-i+[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-s --skip-sequences)*--ignore-sequence=[Won'\''t send these colors sequences]:IGNORE_SEQUENCE:(background foreground cursor color0 color1 color2 color3 color4 color5 color6 color7 color8 color9 color10 color11 color12 color13 color14 color15)' \
'(-d --config-dir)-C+[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-d --config-dir)--config-file=[Use FILE as the config file]:CONFIG_FILE:_files' \
'(-C --config-file)-d+[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'(-C --config-file)--config-dir=[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]:CONFIG_DIR:_files' \
'-q[Don'\''t print anything]' \
'--quiet[Don'\''t print anything]' \
'(-u --update-current -i --ignore-sequence)-s[Skip setting terminal sequences]' \
'(-u --update-current -i --ignore-sequence)--skip-sequences[Skip setting terminal sequences]' \
'-T[Skip templating process]' \
'--skip-templates[Skip templating process]' \
'(-s --skip-sequences)-u[Only update the current terminal]' \
'(-s --skip-sequences)--update-current[Only update the current terminal]' \
'(-C --config-file -d --config-dir)-N[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'(-C --config-file -d --config-dir)--no-config[Uses DIR as the config directory, which holds both \`wallust.toml\` and the templates files (if existent)]' \
'-h[Print help]' \
'--help[Print help]' \
&& ret=0
;;
(help)
_arguments "${_arguments_options[@]}" \
":: :_wallust__help_commands" \
"*::: :->help" \
&& ret=0

    case $state in
    (help)
        words=($line[1] "${words[@]}")
        (( CURRENT += 1 ))
        curcontext="${curcontext%:*:*}:wallust-help-command-$line[1]:"
        case $line[1] in
            (run)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(cs)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(theme)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(migrate)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(debug)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(help)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
        esac
    ;;
esac
;;
        esac
    ;;
esac
}

(( $+functions[_wallust_commands] )) ||
_wallust_commands() {
    local commands; commands=(
'run:Generate a palette from an image' \
'cs:Apply a certain colorscheme' \
'theme:Apply a custom built in theme' \
'migrate:Migrate v2 config to v3 (might lose comments,)' \
'debug:Print information about the program and the enviroment it uses' \
'help:Print this message or the help of the given subcommand(s)' \
    )
    _describe -t commands 'wallust commands' commands "$@"
}
(( $+functions[_wallust__cs_commands] )) ||
_wallust__cs_commands() {
    local commands; commands=()
    _describe -t commands 'wallust cs commands' commands "$@"
}
(( $+functions[_wallust__help__cs_commands] )) ||
_wallust__help__cs_commands() {
    local commands; commands=()
    _describe -t commands 'wallust help cs commands' commands "$@"
}
(( $+functions[_wallust__debug_commands] )) ||
_wallust__debug_commands() {
    local commands; commands=()
    _describe -t commands 'wallust debug commands' commands "$@"
}
(( $+functions[_wallust__help__debug_commands] )) ||
_wallust__help__debug_commands() {
    local commands; commands=()
    _describe -t commands 'wallust help debug commands' commands "$@"
}
(( $+functions[_wallust__help_commands] )) ||
_wallust__help_commands() {
    local commands; commands=(
'run:Generate a palette from an image' \
'cs:Apply a certain colorscheme' \
'theme:Apply a custom built in theme' \
'migrate:Migrate v2 config to v3 (might lose comments,)' \
'debug:Print information about the program and the enviroment it uses' \
'help:Print this message or the help of the given subcommand(s)' \
    )
    _describe -t commands 'wallust help commands' commands "$@"
}
(( $+functions[_wallust__help__help_commands] )) ||
_wallust__help__help_commands() {
    local commands; commands=()
    _describe -t commands 'wallust help help commands' commands "$@"
}
(( $+functions[_wallust__help__migrate_commands] )) ||
_wallust__help__migrate_commands() {
    local commands; commands=()
    _describe -t commands 'wallust help migrate commands' commands "$@"
}
(( $+functions[_wallust__migrate_commands] )) ||
_wallust__migrate_commands() {
    local commands; commands=()
    _describe -t commands 'wallust migrate commands' commands "$@"
}
(( $+functions[_wallust__help__run_commands] )) ||
_wallust__help__run_commands() {
    local commands; commands=()
    _describe -t commands 'wallust help run commands' commands "$@"
}
(( $+functions[_wallust__run_commands] )) ||
_wallust__run_commands() {
    local commands; commands=()
    _describe -t commands 'wallust run commands' commands "$@"
}
(( $+functions[_wallust__help__theme_commands] )) ||
_wallust__help__theme_commands() {
    local commands; commands=()
    _describe -t commands 'wallust help theme commands' commands "$@"
}
(( $+functions[_wallust__theme_commands] )) ||
_wallust__theme_commands() {
    local commands; commands=()
    _describe -t commands 'wallust theme commands' commands "$@"
}

if [ "$funcstack[1]" = "_wallust" ]; then
    _wallust "$@"
else
    compdef _wallust wallust
fi
